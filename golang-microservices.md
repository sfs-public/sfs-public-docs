# Golang microservices

## Web service methods

Web framework - https://github.com/gin-gonic/gin

* All methods stored in ws.go file
* All methods starts with Ws prefix
* All requests have request data struct binded to url or json body `c.ShouldBind...`
* If json or url params not valid - return http.StatusUnprocessableEntity or 422 code. 
* In other cases return 400 code on error
* Use IError struct for error handling

### Example:

*main.go*

```
var router = gin.Default()

func main() {
	router.POST("/test", WsTest)
	router.Run(":7750")
}
```

*ws.go*

```
type ITestRequest struct {
	TestKey string `json:"testKey"`
}

type IError struct {
	Error string `json:"error"`
}

func WsTest(c *gin.Context) {
	var u ITestRequest

	if err := c.ShouldBindJSON(&u); err != nil {
		c.JSON(http.StatusUnprocessableEntity, IError{Error: "Invalid json provided"})
		return
	}

	_, err := SomeMethod(u.TestKey)
	if err != nil {
		c.JSON(400, IError{Error: err.Error()})
		return
	}
	
	c.JSON(http.StatusOK, u)
}

```
